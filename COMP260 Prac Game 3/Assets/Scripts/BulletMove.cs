﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

    // separate speed and direction so we can 
    // tune the speed without changing the code
    public float speed = 10.0f;
    public Vector3 direction;
    private Rigidbody rigidbody;
    public float TTL = 2.0f;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rigidbody.velocity = speed * direction;
    }

    void OnCollisionEnter(Collision collision)
    {
        // Destroy the bullet
        Destroy(gameObject);
    }
    // Update is called once per frame
    void Update () {
        TTL -= Time.deltaTime;
       if (TTL <= 0.0f)
        {
            Destroy();
           }
       
    }
    void Destroy()
    {
    Destroy(gameObject);
    }
}
