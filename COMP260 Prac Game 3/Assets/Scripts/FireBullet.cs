﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {
    public BulletMove bulletPrefab;
    public bool canShoot = true;
    public float shootTimer = 1.0f;
    // Use this for initialization
    void Start () {
	
	}

    void Update()
    {
        if (Time.timeScale == 0)
        {
            return;
        }
        // when the button is pushed, fire a bullet
        if (Input.GetButtonDown("Fire1") && canShoot == true)
        {

            BulletMove bullet = Instantiate(bulletPrefab);
            // the bullet starts at the player's position
            bullet.transform.position = transform.position;

            // create a ray towards the mouse location
            Ray ray =
                Camera.main.ScreenPointToRay(Input.mousePosition);
            bullet.direction = ray.direction;
            canShoot = false;
        }
        if (canShoot == false)
        {
            shootTimer -= Time.deltaTime;
            if (shootTimer <= 0.0f)
            {
                canShoot = true;
                shootTimer = 1.0f;
            }
        }
    }
}
